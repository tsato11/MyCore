object ProjectConfig {
    const val appId = "com.tsato.mobile.mycore"
    const val compileSdk = 33
    const val minSdk = 21
    const val targetSdk = 33
    const val versionCode = 1
    private const val versionMajor = 1
    private const val versionMinor = 0
    private const val versionDB = 0
    const val versionName = "$versionMajor.$versionMinor.$versionDB"
}